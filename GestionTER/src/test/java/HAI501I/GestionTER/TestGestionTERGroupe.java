package HAI501I.GestionTER;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;

class TestGestionTERGroupe {
	Groupe groupecomplet = new Groupe("gc", "Le nom du groupe complet", "sc");
	Groupe groupevide = new Groupe();
	Groupe groupearemplir = new Groupe();
	
	@Test
	public void testToStringGroupe() {
		assertEquals("Le nom du groupe complet (id : gc)\n" + "Le sujet favori du groupe est le : sc", groupecomplet.toString());
		assertEquals(groupecomplet.getNom() + " (id : " + groupecomplet.getId() + ")\n" + "Le sujet favori du groupe est le : " + groupecomplet.getIdSujet(), groupecomplet.toString());
		assertEquals("null (id : null)\n" + "Le sujet favori du groupe est le : null", groupevide.toString());
		assertEquals(groupevide.getNom() + " (id : " + groupevide.getId() + ")\n" + "Le sujet favori du groupe est le : " + groupevide.getIdSujet(), groupevide.toString());
		groupearemplir.setNom("Le nom du groupe a remplir");
		groupearemplir.setVoeux("sar");
		assertEquals("Le nom du groupe a remplir (id : null)\n" + "Le sujet favori du groupe est le : sar", groupearemplir.toString());
	}
	
	@Test
	public void testSerialisationGroupe() throws StreamWriteException, DatabindException, IOException {
		Groupe groupeser = new Groupe("gs", "Groupe à sérialiser", "ids");
		groupeser.serGroupe();
		File fichier = new File("target/groupe"+groupeser.getId()+".json");
	    Scanner lecture = new Scanner(fichier);
	    String l = lecture.nextLine();
	    assertEquals(l, "{\"id\":\"gs\",\"nom\":\"Groupe à sérialiser\",\"idSujet\":\"ids\"}");
	    lecture.close();
	}
	
	@Test
	public void testDeserialisationGroupe() throws StreamWriteException, DatabindException, IOException {
		FileWriter ecriture = new FileWriter("target/groupetest.json");
	    ecriture.write("{\"id\":\"test\",\"nom\":\"Groupe à désérialiser\",\"idSujet\":\"test\"}");
		ecriture.close();
	    Groupe groupedeser = new Groupe();
		groupedeser.deserGroupe("groupetest.json");
		assertEquals("test", groupedeser.getId());
		assertEquals("Groupe à désérialiser", groupedeser.getNom());
		assertEquals("test", groupedeser.getIdSujet());
	}
}