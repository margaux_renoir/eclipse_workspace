package fr.umfds.agl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestAjout {

	@Test
	void testAjout() {
		//On va faire les 3 actions du test
		//Preparation des données : Arrange
		int a = 2;
		int b = 3;
		Calculator calculator = new Calculator();
		//Appel du module que l'on veut tester: Action
		int somme = calculator.add(a,b);
		//Le test du resultat : Assert
		assertEquals(5,somme);
		}
	
	@Test
	void testTest() {
		assertTrue(true);
	}

}
