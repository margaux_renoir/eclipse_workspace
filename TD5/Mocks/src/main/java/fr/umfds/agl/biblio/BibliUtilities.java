package fr.umfds.agl.biblio;

import java.time.Clock;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;

public class BibliUtilities {
    private Clock horloge;
    private GlobalBibliographyAccessInterface GBA = new GlobalBibliographyAccess();

    /*
    retourne des suggestions de lecture "connexes" à ref
    pour l'instant il a été décidé que les notices connexes seraient constituées
    de 5 notices bibliographiques du même auteur que ref, mais de titre différent
    (différent du titre de ref et de plus les 5 notices doivent toutes être de titre différent).
     */
    ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref) {
        HashMap<String, NoticeBibliographique> noticesConnexes = new HashMap<>();
        ArrayList<NoticeBibliographique> noticesMemeAuteur = GBA.noticesDuMemeAuteurQue(ref);

        int i = 0;
        while (i < noticesMemeAuteur.size() && noticesConnexes.size() < 5) {
            NoticeBibliographique actualNotice = noticesMemeAuteur.get(i);
            String titre = actualNotice.getTitre();

            if (!noticesConnexes.containsKey(titre) && titre != ref.getTitre()) {
                noticesConnexes.put(titre, actualNotice);
            }

            i++;
        }

        return  new ArrayList<NoticeBibliographique>(noticesConnexes.values());
    }

    /*
    Ajoute la notice bibliographique dont l'isbn est reçu en paramètre à la liste des notices
    connues de la bibliothèque.
    Si l'isbn ne correspond à aucune notice bibliographique cette méthode jette une
    AjoutImpossibleException.
    Cette méthode renvoie le statut de la notice ajoutée, parmi : newlyAdded (la notice
    n'existait pas), updated (la notice existait et a été modifiée), nochange
    (la notice existait en l'état), notExisting (la notice est null).
    */
    NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException {
        try {
            return Bibliotheque.getInstance().addNotice(GBA.getNoticeFromIsbn(isbn));
        }
        catch (IncorrectIsbnException e) {
            throw new AjoutImpossibleException();
        }
    }

    /*
    Détermine s'il faut prévoir un inventaire
    On prévoit un inventaire si le dernier inventaire était il y a 12 mois ou plus.
    On veillera pour celà à ajouter un attribut représentant l'horloge (Clock) utilisée
    dans BibliUtilities, puis on accèdera à la date courante via LocalDate.now(horloge);
    */
    void prevoirInventaire() {
        LocalDate lastDate = Bibliotheque.getInstance().getLastInventaire();

        if (Period.between(lastDate, LocalDate.now(horloge)).getMonths() >= 12) {
            Bibliotheque.getInstance().inventaire();
        }
    }
}
