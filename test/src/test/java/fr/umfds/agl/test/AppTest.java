package fr.umfds.agl.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import fr.umfds.agl.Calculator;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
	@Test
	void testAjout() {
		//On va faire les 3 actions du test
		//Preparation des données : Arrange
		int a = 2;
		int b = 3;
		Calculator calculator = new Calculator();
		//Appel du module que l'on veut tester: Action
		int somme = calculator.add(a,b);
		//Le test du resultat : Assert
		assertEquals(5,somme);
		}
}
